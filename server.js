const express = require('express');
const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/express-db')
require('dotenv').config();
const port = process.env.PORT || 3000;
const app = express();
app.get('/', (req, res) => {
    res.send('Hello World!');
 });
 app.listen(port);